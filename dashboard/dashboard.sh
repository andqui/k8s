#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

dashboard:create() {
  ${KUBECTL_CMD} create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
  ${KUBECTL_CMD} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/manifests/" #&>/dev/null

  ${KUBECTL_CMD} -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')

  printf "${BLUE}Dashboard enabled.${NC}\n"
}
