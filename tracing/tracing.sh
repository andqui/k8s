#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

tracing:create() {
  allInOne="${1}"

  if [[ -n "${allInOne}" ]]; then
    ${KUBECTL_CMD} create -f https://raw.githubusercontent.com/jaegertracing/jaeger-kubernetes/master/all-in-one/jaeger-all-in-one-template.yml
  else
    local NAMESPACE=tracing
    ${KUBECTL_CMD} create namespace ${NAMESPACE}
    ${KUBECTL_CMD} --namespace ${NAMESPACE} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/jaeger-collector-config-prod.yaml" #&>dev/null
    ${KUBECTL_CMD} --namespace ${NAMESPACE} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/jaeger.yaml" #&>dev/null
  fi
}
