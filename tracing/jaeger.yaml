---
apiVersion: v1
kind: ConfigMap
metadata:
  name: jaeger-configuration-agent
  labels:
    app: jaeger
    jaeger-infra: configuration
data:
  agent: |
    collector:
      host-port: "jaeger-collector:14267"

---
apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: jaeger-agent
  labels:
    app: jaeger
    jaeger-infra: agent-daemonset
spec:
  template:
    metadata:
      labels:
        app: jaeger
        jaeger-infra: agent-instance
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/port: "5778"
    spec:
      containers:
        - name: agent-instance
          image: jaegertracing/jaeger-agent:1.7.0
          args: ["--config-file=/conf/agent.yaml", "--log-level=debug"]
          volumeMounts:
            - name: jaeger-configuration-agent-volume
              mountPath: /conf
          ports:
            - containerPort: 5775
              protocol: UDP
            - containerPort: 6831
              protocol: UDP
            - containerPort: 6832
              protocol: UDP
            - containerPort: 5778
              protocol: TCP
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      volumes:
        - configMap:
            name: jaeger-configuration-agent
            items:
              - key: agent
                path: agent.yaml
          name: jaeger-configuration-agent-volume

---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: jaeger-collector
  labels:
    app: jaeger
    jaeger-infra: collector-deployment
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: jaeger
        jaeger-infra: collector-pod
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/port: "14268"
    spec:
      containers:
        - image: jaegertracing/jaeger-collector:1.7.0
          name: jaeger-collector
          args: ["--config-file=/conf/collector.yaml"]
          ports:
            - containerPort: 14267
              protocol: TCP
            - containerPort: 14268
              protocol: TCP
            - containerPort: 9411
              protocol: TCP
          readinessProbe:
            httpGet:
              path: "/"
              port: 14269
          volumeMounts:
            - name: jaeger-configuration-collector-volume
              mountPath: /conf
          env:
            - name: SPAN_STORAGE_TYPE
              valueFrom:
                configMapKeyRef:
                  name: jaeger-configuration-collector
                  key: span-storage-type
      volumes:
        - configMap:
            name: jaeger-configuration-collector
            items:
              - key: collector
                path: collector.yaml
          name: jaeger-configuration-collector-volume
---
apiVersion: v1
kind: Service
metadata:
  name: jaeger-collector
  labels:
    app: jaeger
    jaeger-infra: collector-service
spec:
  ports:
    - name: jaeger-collector-tchannel
      port: 14267
      protocol: TCP
      targetPort: 14267
    - name: jaeger-collector-http
      port: 14268
      protocol: TCP
      targetPort: 14268
    - name: jaeger-collector-zipkin
      port: 9411
      protocol: TCP
      targetPort: 9411
  selector:
    jaeger-infra: collector-pod
  type: ClusterIP

---
apiVersion: v1
kind: Service
metadata:
  name: zipkin
  labels:
    app: jaeger
    jaeger-infra: zipkin-service
spec:
  ports:
    - name: jaeger-collector-zipkin
      port: 9411
      protocol: TCP
      targetPort: 9411
  selector:
    jaeger-infra: collector-pod
  type: ClusterIP
