#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

ingress:create() {
  local cert_arn="${1}"
  printf "Creating ${BLUE}nginx-ingress-controller${NC}\n"
  local ingress_service_name="ingress-nginx"
  ${KUBECTL_CMD} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/manifests/" #&>/dev/null

  # Cannot update to latest until this is fixed if ssl force redirect is supposed to work
  # https://github.com/kubernetes/ingress-nginx/issues/2724
  # First the mandatory part
  #${KUBECTL_CMD} apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml &>/dev/null

  # And then the AWS specific parts
  #${KUBECTL_CMD} apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/aws/service-l7.yaml
  #${KUBECTL_CMD} apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/aws/patch-configmap-l7.yaml

  until ${KUBECTL_CMD} get service ${ingress_service_name} --namespace ${ingress_service_name} &>/dev/null ; do date; sleep 1; echo ""; done

  # Replace the dummy cert arn with the real one
  if [[ -n "${cert_arn}" ]]; then
    ${KUBECTL_CMD} annotate service \
        --overwrite \
        --namespace ${ingress_service_name} \
        ${ingress_service_name} \
         "service.beta.kubernetes.io/aws-load-balancer-ssl-cert"="${cert_arn}" \
         "service.beta.kubernetes.io/aws-load-balancer-backend-protocol"="http" \
         "service.beta.kubernetes.io/aws-load-balancer-ssl-ports"="https" &>/dev/null
  fi
  printf "Created ${BLUE}nginx-ingress-controller${NC}\n"

}
